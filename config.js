module.exports = {
  host      : process.env.DXF_TOOLS_DB_HOST || process.env.DXF_IMPORTER_DB_HOST,
  port      : process.env.DXF_TOOLS_DB_PORT || 5432,
  user      : process.env.DXF_TOOLS_DB_USER || process.env.DXF_IMPORTER_DB_USER,
  password  : process.env.DXF_TOOLS_DB_PASSWORD || process.env.DXF_IMPORTER_DB_PASSWORD,
  database  : process.env.NODE_ENV === 'test' ? 'test' : (process.env.DXF_TOOLS_DB_NAME || process.env.DXF_IMPORTER_DB_NAME)
};
