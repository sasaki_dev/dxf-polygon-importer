var ogr2ogr = require('ogr2ogr'),
    lineToPolygon = require('turf-line-to-polygon'),
    flatlands = require('flatlands'),
    wkt = require('terraformer-wkt-parser'),
    Knex = require('knex'),
    path = require('path'),
    _ = require('lodash'),
    RSVP = require('rsvp');

var config = require('./config');


/**
 * Instantiates a new Importer class.
 * @param  {String}   file   DXF file path
 * @param  {Object}          options Configuration object:
                                    {
                                      polygonLayer: String,
                                      labelLayer: String,
                                      project: String
                                    }
 */
function Importer (file, options) {
  if (!(this instanceof Importer)) return new Importer (file, options);

  if (!file) {
    throw new Error('A dxf file path or stream is required.');
  }
  if (!options.polygonLayer || !options.labelLayer) {
    throw new Error('Polygon- and Labellayer are required.');
  }
  if (!options.project) {
    throw new Error('Project is required.');
  }

  this._file = file;
  this._filename = path.basename(file);
  this._polygonLayer = options.polygonLayer;
  this._labelLayer = options.labelLayer;
  this._project = options.project;
  this._version = 1;
  this._featureTypes = ['polygon', 'label'];

  this._dbClient = Knex({
    client: 'pg',
    connection: {
      host: config.host,
      port: config.port,
      user: config.user,
      password: config.password,
      database: config.database
    }
  });

  return this;
}



/**
 * Initiates the import process with schema-setup, polyline-conversions and
 * sql-inserts.
 * @param  {Boolean} keepPool  Flag if database connection pool should be
 *                             kept alive or torn down after import.
 *                             Default: falsy, 'undefined'
 * @return {Promise}           Resolves into object with filename, project
 *                             and version
 */
Importer.prototype.go = function (keepPool) {
  var that = this;

  return that.initDbSchema()

  .then(function () {
    var promise;

    promise = new RSVP.Promise(function(resolve, reject) {
      ogr2ogr(that._file).project('EPSG:3857', 'EPSG:3857').exec(function (err, data) {
        var rawFeatures, sqlValues,
            types = {};

        if (err) reject(err);

        types[that._polygonLayer] = 'polygon';
        types[that._labelLayer] = 'label';

        rawFeatures = _.filter(data.features, function (f) {
          return _.contains([that._polygonLayer, that._labelLayer], f.properties.Layer);
        });

        sqlValues = _.map(rawFeatures, function (f) {
          var geometry, properties, type,
              values;

          geometry = that.isPotentialPolygon(f.geometry) ? lineToPolygon(f.geometry) : f.geometry;
          type = (geometry.type === 'LineString') ? 'unknown' :  types[f.properties.Layer];
          properties = (type === 'label') ? { text: f.properties.Text } : {};

          values = [
            that.sqlize(that._filename),
            that._version,
            that.sqlize(that._project),
            that.sqlize(type),
            that.sqlize(JSON.stringify(properties)),
            'ST_GeomFromText(\'' + wkt.convert(flatlands(geometry)) + '\', 3857)'
          ];
          return values.join(',');

        });

        resolve(sqlValues);
      });
    });

    return promise;
  })

  // SQL insert
  .then(function (features) {
    return that._dbClient.raw('INSERT INTO dxf_features ( filename, version, project, type, validations, geom ) VALUES (' + features.join('),(') + ');');
  })

  .then(function () {
    return { filename: that._filename, project: that._project, version: that._version };
  })

  // end database connection pool
  .finally(function () {
    if (!keepPool) that._dbClient.destroy();
  });
};



/**
 * Takes a LineString and checks if it could be converted to a valid polygon
 * @param   {[type]}   geom GeoJSON geometry to test
 * @return  {Boolean}  Flag for potential polygon
 */
Importer.prototype.isPotentialPolygon = function (geom) {
  if (geom.type !== 'LineString') return false;
  if (geom.coordinates.length <= 2) return false;
  // looks like a line in CAD: 3 points line with same start and end location
  if (geom.coordinates.length === 3 &&
    _.isEqual(geom.coordinates[0], geom.coordinates[2])) return false;

  return true;
};



/**
 * Get current version number
 * @return  {Promise}  Resolves into version number
 */
Importer.prototype.getVersion = function () {
  var that = this;

  return that._dbClient('dxf_features')
    .where({
      'filename': that._filename,
      'project': that._project
    })
    .max('version as version')

  .then(function (rows) {
    return rows[0].version;
  });
};



/**
 * Initialize table structure and set incremented version.
 * A version represents one project+filename import.
 * @return  {Promise} version  Resolves into incremented version number
 */
Importer.prototype.initDbSchema = function () {
  var that = this;

  return that._dbClient.schema.hasTable('dxf_features')

  .then(function (exists) {
    if (!exists) {
      return that._dbClient.schema.createTable('dxf_features', function (t) {
        t.increments('id').primary();
        t.text('name');
        t.string('filename');
        t.integer('version');
        t.string('project');
        t.string('type');
        t.json('validations');
        t.specificType('geom', 'GEOMETRY(GEOMETRY,3857)');
      });
    } else {
      // table exists, get latest version and increment it
      return that.getVersion().then(function (v) {
         that._version = v + 1;
      });
    }
  })

  // initialize views for latest version of each feature type
  .then(function () {
    var promises;

    promises = _.map(that._featureTypes, function (e) {
      return that._dbClient.raw('CREATE OR REPLACE VIEW all_' + e + 's AS WITH latest AS ( SELECT filename, max(version) AS version FROM dxf_features GROUP BY filename ) SELECT * FROM dxf_features AS v WHERE version = (SELECT version FROM latest WHERE filename = v.filename) AND type = \'' + e + '\' ORDER BY filename;');
    });

    return RSVP.all(promises);
  });
};



/**
 * Alias to end database connection pool
 * @return {Promise}
 */
Importer.prototype.destroy = function () {
  return this._dbClient.destroy();
};



/**
 * Wraps a string in quotes to be used in SQL statements
 * @param  {String} s String
 * @return {String}   String wrapped in quotes
 */
Importer.prototype.sqlize = function (s) {
  return '\'' + s + '\'';
};


module.exports = Importer;
