[ ![Codeship Status for sasaki_dev/dxf-polygon-importer](https://www.codeship.io/projects/882923d0-df48-0131-65e7-06b997a9e467/status)](https://www.codeship.io/projects/24947)

# DXF Polygon Importer

Parses a DXF file and imports closed 'CAD Polylines' as polygons and labels as points into a PostGIS table. The imports will be versioned based on a project identifier and filename combination.

## Installation and requirements

    npm install dxf-polygon-importer

The module **requires** [ogr2ogr][1] and a [PostGIS][2] enabled PostgreSQL database connection.

Following environment variables need to be set:

    DXF_TOOLS_DB_HOST      ...  PostgreSQL server host
    DXF_TOOLS_DB_USER      ...  Database user name
    DXF_TOOLS_DB_PASSWORD  ...  Database user password
    DXF_TOOLS_DB_NAME      ...  Database name

The module creates following database tables and views:

    dxf_features [table]   ...  all imported geometries
    all_polygons [view]    ...  last version of imported polygons
    all_labels   [view]    ...  last version of imported label points

## Usage

    var Importer = require('dxf-polygon-importer');

    Importer('mydrawing.dxf', {
      polygonLayer: 'MYPOLYLINES',
      labelLayer: 'MYNAMES',
      project: 'MyProject'
    }).go().then(function (data) {
      // handle data
      // { filename: 'mydrawing.dxf', project: 'MyProject', version: 1 }
    });

## API

### initialize `Importer(file, options)`

The class takes a DXF file path as first `file` argument and `options` object with following properties as second:

    {
      polygonLayer: String,  ...  CAD layer containing polygons
      labelLayer: String,    ...  CAD layer containing labels
      project: String        ...  Unique project identifier
    }

### go `Importer.go(keepPool)`

Initiates the import process with schema-setup, polyline-conversions and sql-inserts. The method takes a `keepPool` Boolean argument to indicate if database connection pool should be kept alive or torn down after import. This can be useful if the pool should be used for multiple subsequent imports. Default value is falsy, 'undefined'.

### destroy `Importer.destroy()`

Alias to terminate database connection pool.


[1]: http://www.gdal.org/index.html
[2]: http://postgis.net/
