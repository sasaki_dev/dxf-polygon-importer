var Importer = require('../.'),
    config = require('../config'),
    test = require('tape'),
    Knex = require('knex');


var dbClient = Knex({
  client: 'pg',
  connection: {
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    database: config.database
  }
});


test('importer', function(t){
  var importer;

  t.plan(9);

  importer = Importer('./test/test.dxf', {
    polygonLayer: 'PLINE',
    labelLayer: 'NUM',
    project: 'test'
  });

  t.equal(typeof importer, 'object', 'Importer is OK');
  t.equal(typeof importer.go, 'function', 'Importer go is OK');
  t.equal(typeof importer.destroy, 'function', 'Importer destroy is OK');

  // clean up previouse test
  dbClient.raw('DROP TABLE IF EXISTS dxf_features CASCADE;')

  .then(function () {
    return importer.go(true);
  })

  // basic import
  .then(function (data) {
    t.equal(data.version, 1, 'version is OK');
    t.equal(data.filename, 'test.dxf', 'filename is OK');
    t.equal(data.project, 'test', 'project is OK');
  })

  // views
  .then(function () {
    return dbClient('all_polygons').count();
  })
  .then(function (rows) {
    t.equal(rows[0].count, '20', 'polygons count is OK');
    return dbClient('all_labels').count();
  })
  .then(function (rows) {
    t.equal(rows[0].count, '23', 'labels count is OK');
  })

  // versioning
  .then(function () {
    return importer.go(true);
  })
  .then(function () {
    return importer.getVersion();
  })
  .then(function (version) {
    t.equal(version, 2, 'versioning is OK');
    return importer.destroy();
  })

  // the end
  .finally(function () {
    dbClient.destroy();
  });
});
